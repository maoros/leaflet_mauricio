let mymap = L.map('mapid');
mymap.setView([4.579, -74.133], 18);
let provider = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});

provider.addTo(mymap);
let maker = L.marker([4.579, -74.133]).addTo(mymap);
let maker1 = L.marker([4.57989, -74.13356]).addTo(mymap);
